import 'package:flutter/material.dart';
import './question.dart';

void main() {
  runApp(QuizPractice());
}

class QuizPractice extends StatefulWidget {
  @override
  _QuizPracticeState createState() => _QuizPracticeState();
}

class _QuizPracticeState extends State<QuizPractice> {
  var wordList = ['v1', 'v2', 'v3', 'v4'];
  var numv = 0;
  void funtorun(int i) {
    print(wordList[numv]);
    if (numv != 3) {
      setState(() {
        numv++;
      });
    } else {
      setState(() {
        numv = 0;
      });
    }
    print(i);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("testy mctestface"),
      ),
      body: Column(
        children: <Widget>[
          Question(
            questionText: wordList[numv],
          ),
          RaisedButton(
            child: Text('data'),
            onPressed: () => {funtorun(3)},
          )
        ],
      ),
    ));
  }
}
